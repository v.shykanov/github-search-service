<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api',
    //'middleware' => 'auth:api',
    'as' => 'api.',
], function () {
    Route::apiResource('github-users', 'GitHubUsersController');
    Route::post('github-users/search', 'GitHubUsersController@search')
        ->name('github-users.search');
});
