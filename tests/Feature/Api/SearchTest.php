<?php

namespace Tests\Feature\Api;

use App\Jobs\StoreGitHubUsers;
use App\Models\GitHubUser;
use App\Models\User;
use Tests\AbstractTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends AbstractTestCase
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->instance('clients.github', \Mockery::mock('client', function ($mock) {
            $mock->shouldReceive('search->users')
                ->andReturn([
                    'items' => [
                        [
                            'login' => 'foobar',
                            'avatar_url' => 'foobar',
                            'id' => 1,
                        ]
                    ]
                ]);
        }));
    }

    /**
     * Should search for github users and return results
     *
     * @return void
     */
    public function testShouldSearchGitHubUsersAndReturnResults(): void
    {
        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(route('api.github-users.search'), [
                'term' => 'foo'
            ]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'username' => 'foobar',
            'avatar_url' => 'foobar',
            'github_id' => 1,
        ]);
    }

    /**
     * Should store users received from github api
     *
     * @return void
     */
    public function testShouldStoreUsersReceivedFromGitHubApi(): void
    {
        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(route('api.github-users.search'), [
                'term' => 'foo'
            ]);

        $this->assertDatabaseHas('github_users', [
            'username' => 'foobar',
            'avatar_url' => 'foobar',
            'github_id' => 1,
        ]);
        $this->assertCount(1, GitHubUser::all());
    }

    /**
     * Should store only unique users received from github api
     *
     * @return void
     */
    public function testShouldStoreOnlyUniqueUsersReceivedFromGitHubApi(): void
    {
        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(route('api.github-users.search'), [
                'term' => 'foo'
            ]);

        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(route('api.github-users.search'), [
                'term' => 'foo'
            ]);

        $this->assertDatabaseHas('github_users', [
            'username' => 'foobar',
            'avatar_url' => 'foobar',
            'github_id' => 1,
        ]);
        $this->assertCount(1, GitHubUser::all());
    }
}
