<?php
declare(strict_types=1);

namespace App\Http\Requests\Api\GitHubUsers;

use App\Http\Requests\AbstractApiRequest;

final class Update extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'is_hide' => ['bool'],
        ];
    }
}
