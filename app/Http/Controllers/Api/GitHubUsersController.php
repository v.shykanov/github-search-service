<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\GitHubUsers\Update as UpdateGitHubUsersRequest;
use App\Http\Requests\Api\GitHubUsers\Search as SearchGitHubUsersRequest;
use App\Http\Resources\GitHubUsers as GitHubUsersResource;
use App\Models\GitHubUser;
use App\Models\GitHubUser as GitHubUsersModel;
use App\Services\GitHubGateway;
use App\Services\GitHubService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Resource as ResourceService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;
use Psr\Container\ContainerInterface;

final class GitHubUsersController extends Controller
{
    /**
     * @var GitHubGateway
     */
    protected $gitHubGateway;

    /**
     * @var GitHubService
     */
    protected $gitHubService;

    /**
     * @param GitHubGateway $gitHubGateway
     * @param GitHubService $gitHubService
     */
    public function __construct(GitHubGateway $gitHubGateway, GitHubService $gitHubService)
    {
        $this->gitHubGateway = $gitHubGateway;
        $this->gitHubService = $gitHubService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return GitHubUsersResource::collection(
            GitHubUsersModel::where('is_hide', false)->get()
        );
    }

    /**
     * @param ShowGitHubUsersRequest $request
     * @param string $id
     *
     * @return GitHubUsersResource
     */
    public function show(Request $request, string $id): GitHubUsersResource
    {
        GitHubUsersResource::withoutWrapping();

        return GitHubUsersResource::make(GitHubUser::findOrFail($id));
    }

    /**
     * @param \App\Http\Requests\Api\GitHubUsers\Update $request
     * @param string $id
     *
     * @return GitHubUsersResource
     */
    public function update(UpdateGitHubUsersRequest $request, string $id): GitHubUsersResource
    {
        $gitHubUser = GitHubUser::findOrFail($id);
        $this->gitHubService->update($gitHubUser, $request->only([
            'is_hide',
        ]));

        GitHubUsersResource::withoutWrapping();

        return GitHubUsersResource::make($gitHubUser);
    }

    /**
     * @param \App\Http\Requests\Api\GitHubUsers\Search $request
     *
     * @return array|\Illuminate\Support\Collection|null
     */
    public function search(SearchGitHubUsersRequest $request)
    {
        $collection = $this->gitHubGateway->search(\request('term'));

        return GitHubUsersResource::collection(
            $collection->where('is_hide', false)
        );
    }
}

