<?php

namespace App\Providers;

use Cache\Adapter\Illuminate\IlluminateCachePool;
use Illuminate\Cache\DatabaseStore;
use Illuminate\Cache\RedisStore;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;
use Github\Client as GitHubClient;

class GitHubClientProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('clients.github', function () {
            $client = new GitHubClient();
            //$client->addCache(new IlluminateCachePool(new RedisStore(
            //    resolve(RedisManager::class)
            //)));

            return $client;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
