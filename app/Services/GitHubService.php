<?php

namespace App\Services;

use App\Models\GitHubUser;
use Illuminate\Support\Collection;

class GitHubService
{
    /**
     * @param \App\Models\GitHubUser $gitHubUser
     * @param array $attributes
     *
     * @return bool
     */
    public function update(GitHubUser $gitHubUser, array $attributes): bool
    {
        return $gitHubUser->update($attributes);
    }

    /**
     * @param \Illuminate\Support\Collection $usersCollection
     *
     * @return \Illuminate\Support\Collection
     */
    public function updateOrCreateManyUsers(Collection $usersCollection): Collection
    {
        $collection = collect();
        foreach ($usersCollection as $user) {
            $user = GitHubUser::updateOrCreate([
                'github_id' => $user['github_id']
            ], $user);
            $collection->push($user);
        }

        return $collection;
    }
}
